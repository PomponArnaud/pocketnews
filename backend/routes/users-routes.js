import express from 'express';
import pool from '../db.js';
import bcrypt from 'bcrypt';
import { authenticateToken } from '../middleware/authorization.js';
import { utilLogin } from '../utils/util-login.js';

const router = express.Router();

router.get('/',authenticateToken, async (req, res) => {
    try {
        const users = await pool.query('SELECT * FROM users');
        res.json({users : users.rows});
    } catch (error) {
        res.status(500).json({error:error.message});
    }
});

router.post('/register', async (req, res) => {
    try {
        if(!utilLogin.validateEmail(req.body.email)) throw new Error("Invalid email.")
        const hashedPassword = await bcrypt.hash(req.body.password,10);
        const newUser = await pool.query('INSERT INTO users (user_name,user_email,user_password,user_role) VALUES ($1,$2,$3,$4) RETURNING *',
            [req.body.name,req.body.email,hashedPassword,req.body.role]);
        res.json({users:newUser.rows[0]});
    } catch (error) {
        res.status(500).json({error:error.message});
    }
})

export default router;