import pg from 'pg';
const { Pool } = pg;

let localPoolConfig = {
    user:process.env.USER_DB,
    password:process.env.PASSWORD_DB,
    host:process.env.HOST_DB,
    port:process.env.PORT_DB,
    database:process.env.DATABASE_DB
};

const poolConfig = process.env.DATABASE_URL ? { 
    connectionString: process.env.DATABASE_URL, 
    ssl: { rejectUnauthorized: false } 
} : localPoolConfig;

const pool = new Pool (poolConfig);
export default pool;