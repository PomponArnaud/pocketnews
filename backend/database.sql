CREATE DATABASE project_marius;

CREATE TABLE users (
    user_id SERIAL PRIMARY KEY,
    user_name VARCHAR ( 50 ) NOT NULL,
    user_email VARCHAR ( 75 ) UNIQUE NOT NULL,
    user_password VARCHAR ( 150 ) NOT NULL,
    user_role INT NOT NULL
);

SELECT * FROM users;

INSERT INTO users (user_name,user_email,user_password,user_role) VALUES ('Arnaud','arnaud@gmail.com','arnaud','1');
INSERT INTO users (user_name,user_email,user_password,user_role) VALUES ('Marius','marius@gmail.com','marius','1');

--su arnaud
--psql -d project_marius
--\conninfo